#!/bin/bash -e
#
# Author:: Lance Albertson <lance@osuosl.org>
# Copyright:: Copyright 2019-2020, Cinc Project
# License:: Apache License, Version 2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This will patch Chef using Cinc branded patches
git_patch() {
  if [ -n "${2}" ] ; then
    CINC_BRANCH="${2}"
  elif [ "${REF}" == "main" -o -z "${REF}" ] ; then
    CINC_BRANCH="stable/cinc"
  else
    CINC_BRANCH="stable/cinc-${REF}"
  fi
  if [ -n "${3}" ] ; then
    REPO="${3}"
  else
    REPO="https://gitlab.com/cinc-project/upstream/${1}.git"
  fi
  echo "Patching ${REPO} from ${CINC_BRANCH}..."
  git remote add -f --no-tags -t ${CINC_BRANCH} cinc ${REPO}
  git merge --no-edit cinc/${CINC_BRANCH}
}

TOP_DIR="$(pwd)"
source /home/omnibus/load-omnibus-toolchain.sh
set -ex
# remove any previous builds
rm -rf chef-server omnibus-software
git config --global user.email || git config --global user.email "maintainers@cinc.sh"
echo "Cloning ${REF:-main} branch from ${ORIGIN:-https://github.com/chef/chef-server.git}"
git clone -q -b ${REF:-main} ${ORIGIN:-https://github.com/chef/chef-server.git}
cd chef-server
git_patch chef-server ${CINC_REF} ${CINC_REPO}
gem install -N bundler -v 2.2.22
cd omnibus
ruby ${TOP_DIR}/scripts/checkout.rb -n omnibus-software -p $TOP_DIR
cd $TOP_DIR/omnibus-software
git_patch omnibus-software stable/cinc
cd $TOP_DIR
echo "Copying Cinc resources..."
cp -rp cinc-server/cinc/cinc-server-wrapper chef-server/omnibus/files/private-chef-scripts/cinc-server-wrapper

ruby scripts/omnibus-fixes.rb
echo "cache_dir '${TOP_DIR}/cache'" >> chef-server/omnibus/omnibus.rb
mkdir -p ${TOP_DIR}/cache
if [ "${GIT_CACHE}" == "true" ] ; then
  mkdir -p ${TOP_DIR}/cache/git_cache
  echo "git_cache_dir '${TOP_DIR}/cache/git_cache'" >> chef-server/omnibus/omnibus.rb
  echo "use_git_caching true" >> chef-server/omnibus/omnibus.rb
else
  echo "git cache has been disabled"
fi

# Update Gemfile.lock for Cinc dependencies to allow bundle to work properly.
gem install -N bundler -v 2.1.4
gem install -N bundler -v 2.2.15
gem install -N bundler -v 2.2.19
gem install -N bundler -v 2.3.7
gem install -N bundler -v 2.2.22
cd ${TOP_DIR}/chef-server/oc-chef-pedant
bundle _2.2.19_ lock --conservative --update chef chef-zero inspec-core
cd ${TOP_DIR}/chef-server/src/chef-server-ctl
bundle _2.1.4_ lock --conservative --update chef chef-utils chef-config \
  chef-zero inspec-core mixlib-install chef_backup omnibus-ctl knife
cd ${TOP_DIR}/chef-server/src/oc-id
bundle _2.3.7_ lock --conservative --update chef
cd ${TOP_DIR}/chef-server/omnibus/partybus
bundle _2.2.15_ lock --conservative --update chef-utils
# TODO: Temp fix until this gets bumped upstream
cd ${TOP_DIR}/chef-server/omnibus
bundle _2.2.22_ lock --conservative --update license_scout

#!/bin/bash
set -ueo pipefail

cleanup() {
  echo
  echo "--- Cleanup triggered, copying system log files"
  set +e
  mkdir -p /vagrant/logs/system/{var,etc}
  if [ -e /usr/bin/rsync ] ; then
    rsync -aH /var/log /vagrant/logs/system/var
    rsync -aH /etc/cinc-project /vagrant/logs/system/etc
  else
    cp -r /var/log /vagrant/logs/system/var
    cp -r /etc/cinc-project /vagrant/logs/system/etc
  fi

  if [ -e /var/opt/cinc-backup ] ; then
    cp -a /var/opt/cinc-backup/cinc-backup-*.tgz /vagrant/logs/system/
  fi
  echo
}

trap cleanup EXIT

export PATH="/opt/cinc-project/bin:/opt/cinc-project/embedded/bin:$PATH"

hostnamectl set-hostname localhost.localdomain

echo
echo "--- Installing rsync for backup/restore testing"
echo

if [ -e /usr/bin/dnf ] ; then
  dnf install -y rsync
elif [ -e /usr/bin/yum ] ; then
  yum install -y rsync
elif [ -e /usr/bin/apt ] ; then
  apt-get update
  apt-get -y install rsync
elif [ -e /usr/bin/zypper ] ; then
  zypper install -y rsync
fi

echo
echo "--- Running 'cinc-server-ctl version'"
echo

cinc-server-ctl version

echo
echo "--- Running 'chef-server-ctl version'"
echo

chef-server-ctl version

echo
echo "--- Reconfiguring cinc-server"

mkdir -p /etc/cinc-project /vagrant/logs /tmp/cinc-staging

cat << EOF > /etc/cinc-project/cinc-server.rb
opscode_erchef['keygen_start_size'] = 30
opscode_erchef['keygen_cache_size'] = 60
nginx['ssl_dhparam'] = '/etc/cinc-project/dhparam.pem'
insecure_addon_compat false
data_collector['token'] = 'foobar'
EOF

cat << EOF > /etc/cinc-project/dhparam.pem
-----BEGIN DH PARAMETERS-----
MIIBCAKCAQEAtAvx3pUHBNcK2nD58nPPlKtJzZvrFCyKEn9BSn16/BmFwBhL8rh4
+fkrnLflZ/k9wJjiUkU0DCi+Fy6DUohPHOmmT0BiuwgsDZAFDyTj0PeZKINpbHnQ
EbZENzWo5s5hsb1zVxIMEtTMRrigdHM3FQupFbzOHxonkO0JlocarOJBHGX+Crjp
y/8SReCpC71R+Vl6d4+Dw6GFdL+6k6W558dPfq3UeV8HPWQEaM7/jXDUKJZ0tB6a
1csrekkz3gBFlSjSxececRVn8bm5dTfc86rIWJWeWQVLYdBFT6zi43AvF+nLYKYh
+oVnVrhWgOLYvEKX311d9SaqcdrXVFscYwIBAg==
-----END DH PARAMETERS-----
EOF

cinc-server-ctl reconfigure > /vagrant/logs/reconfigure.log 2>&1

echo
echo "--- Sleeping (120 seconds) to let the system settle"
sleep 120

echo
echo "--- Running 'cinc-server-ctl test'"

cinc-server-ctl test -J /vagrant/logs/pedant.xml --all \
  > /vagrant/logs/pedant.log 2>&1

echo
echo "--- Reconfiguring cinc-server in FIPS mode"

echo "fips true" >> /etc/cinc-project/cinc-server.rb

cinc-server-ctl reconfigure > /vagrant/logs/reconfigure-fips.log 2>&1

echo
echo "--- Sleeping (120 seconds) to allow the Chef Server to reconfigure in FIPS mode"
sleep 120

echo
echo "--- Running 'cinc-server-ctl test' with FIPS mode"

cinc-server-ctl test -J /vagrant/logs/pedant-fips.xml --smoke \
  > /vagrant/logs/pedant-fips.log 2>&1

echo
echo "--- Running 'cinc-server-ctl backup'"
cinc-server-ctl backup -y > /vagrant/logs/backup.log 2>&1

echo
echo "--- Running 'cinc-server-ctl restore'"
cinc-server-ctl restore -d /tmp/cinc-staging/ \
  -c /var/opt/cinc-backup/cinc-backup-*.tgz > /vagrant/logs/restore.log 2>&1

echo
echo "--- Running Cinc Auditor tests"

/opt/cinc-project/embedded/bin/cinc-auditor exec \
  /vagrant/test/integration/cinc-tests/ \
  --no-distinct-exit \
  --reporter cli \
  junit:/vagrant/logs/auditor.xml
